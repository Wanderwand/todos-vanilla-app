const apiUrl = "https://todos-nest-api.herokuapp.com"


let newTodo = {
  name : "Empty Task" ,
  dateCompletion : new Date() ,
  completed : false  
}

const todoList = [] ;
// 
const allTodos = document.querySelector("#allTodos")

function setTodo (e){
  newTodo.name = e.target.value;
  console.log(newTodo.name)
  console.log(e.target.value)
}

function setDateCompletion (e){
  newTodo.dateCompletion = new Date(e.target.value)
}

function addTodo (e){ 
  // post to backend 
  const xhttp = new XMLHttpRequest();
  xhttp.onreadystatechange =function (){
    console.log("sent")

    if (this.readyState == 4 ) {
      const result = (JSON.parse(this.responseText))
      // console.log(result)
      const allTodos=document.querySelector("#allTodos")
      const i = allTodos.lastChild && allTodos.lastChild.attributes ? allTodos.lastChild.attributes.key : 0;
      
      appendTodo(result,i)
      newTodo.name="new task";
      newTodo.dateCompletion=new Date();
      newTodo.completed=false;
    }
  }
  xhttp.open("POST", `${apiUrl}/todo`, true)
  xhttp.setRequestHeader('Content-type', 'application/json');
  console.log(newTodo)

  xhttp.send(JSON.stringify(newTodo));

}
function deleteTodo(e){
  const i= parseInt(e.target.attributes[1].nodeValue)
  console.log((i))
   var url = `${apiUrl}/todo/`;
    var xhr = new XMLHttpRequest();
    xhr.open("DELETE", url+i , true);
    xhr.onload = function () {
        var res = (xhr.responseText);
        if (xhr.readyState == 4 && xhr.status == "200") {
            console.table(res);
            const allTodos=document.querySelector("#allTodos")
            allTodos.removeChild( allTodos.querySelector(`#listWrapper_${i}[key="${i}"]`))
            console.log("deleted")

        } else {
            console.error(res);
        }
    }
    xhr.send(null);
}

// functions related to appendtodo
function onChangeStatus(event,i){
  const id= parseInt(e.target.attributes[1].nodeValue)
console.log(id)
  // todoList[i].completed =!todoList[i].completed
  // const listWrapper =document.querySelector(`#listWrapper_${i}`)
  // console.log(listWrapper)
  // listWrapper.querySelector("#status").innerHTML=`${todoList[i].completed? "Completed":"Incomplete"}`
  // todoList[i].completed ? 
  // listWrapper.classList.add("bg-gray-200","italic") :
  // listWrapper.classList.remove("bg-gray-200","italic")  
}



// created date
const createdDate =(todo)=>{
  const date = new Date(todo.dateCreated);
  return date.toLocaleString()
}
// completion date 
const completionDate = (todo)=>{
  const date = new Date(todo.dateCompletion);
  return date.toLocaleString()
}


//function appendChild

function appendTodo(todo,i){
  allTodos.innerHTML+= 
  `<div id="listWrapper_${todo.id}" key=${todo.id} class="m-2 p-2 w-3/4 md:flex shadow rounded justify-center items-center ${todo.completed ? "bg-gray-200 italic"  : ""}">
    <input id="checkbox" type="checkbox" ${todo.completed?"checked=true" : ""}  onchange="onChangeStatus(event,${i})" class="block mx-auto mb-3 "></input>
    <div id="wrapper" class="flex flex-wrap flex-grow-3 px-4 " >
      <div id="nameWrapper" class="flex-grow-3 text-left">                    
        <div id="name" class= "flex-4 ">${todo.name}</div>
        <div id="status" class="text-gray-500 ">${todo.completed? "Completed":"Incomplete"}</div>
      </div>
      <div class="text-left flex-grow-1">
        <div id="cratedDate" class="text-gray-700 ">Created :<b class="text-gray-700"> ${createdDate(todo)}</b> </div>
        <div id="conpletionDate" class="text-gray-700">Complete Till : <b>${completionDate(todo)} </b></div>
      </div>
    </div>
    <button onclick="deleteTodo(event)" key=${todo.id} class="block mx-auto rounded border-gray-400 text-gray-400 w-8 h-8 px-2 m-2 hover:text-white hover:bg-gray-300">
      x 
    </button>
  </div>`  
}


// on window load get all todos from backed 
function getAllTodos(){
  console.log("get req called")
  // get request to bakcend 
  const xhttp = new XMLHttpRequest();
  xhttp.onreadystatechange =function (){
    if (this.readyState == 4 && this.status == 200) {
      let todos = JSON.parse(this.responseText)
      todos.forEach((todo,i)=>{appendTodo(todo,i); todoList.push(todo)})
    }
  }
  xhttp.open("GET", `${apiUrl}/todo`,true)
  xhttp.send();



} 
window.onload=(event)=>{
  console.log("page is loaded")
  getAllTodos()
   newTodo.name="New task ";
      newTodo.dateCompletion=new Date();
      newTodo.completed=false;
}
